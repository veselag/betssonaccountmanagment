﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using DataAccess.Interface;
using DataAccess.Model;

namespace BetssonAccountManagment
{
    [TestClass]
    public class TransferRepositoryTest
    {
        [TestMethod]
        public void CreateNew_Should_Return_ID()
        {
            var transferRepository = new Mock<ITransferRepository>();
            var id = Guid.NewGuid();
            var resultId = id;
            var accountId = Guid.NewGuid();
            var transfer = new Transfer()
            {
                Id = id,
                AccountId = accountId,
                Amount = 0.0M,
                Type = TransferType.Credit,
                TransferTime = DateTime.Now,
                Status = TransferStatus.NotSaved,
            };
            transferRepository.Setup(c => c.Add(transfer)).Returns(resultId);
            Assert.AreEqual(id, resultId);
        }
        [TestMethod]
        public void Save_Should_Return_Satus()
        {
            var transferRepository = new Mock<ITransferRepository>();
            var id = Guid.NewGuid();
            var result = TransferStatus.SavedOnServer;
            var accountId = Guid.NewGuid();
            var transfer = new Transfer()
            {
                Id = id,
                AccountId = accountId,
                Amount = 0.0M,
                Type = TransferType.Credit,
                TransferTime = DateTime.Now,
                Status = TransferStatus.NotSaved,
            };
            transferRepository.Setup(c => c.Update(transfer)).Returns(result);
            Assert.AreEqual(TransferStatus.SavedOnServer, result);
        }
    }
}
