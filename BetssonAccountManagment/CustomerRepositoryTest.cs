﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataAccess.Model;
using DataAccess.Interface;
using DataAccess.Implementation;
using Moq;

namespace BetssonAccountManagment
{
    [TestClass]
    public class CustomerRepositoryTest
    {
        [TestMethod]
        public void CreateNew_Should_Return_ID()
        {
            var customerRepository = new Mock<ICustomerRepository>();
            var id = Guid.NewGuid();
            var resultId = id;
            var customer = new Customer()
            {
                Id = id,
                Name ="",
                IsActive = true
            };
            customerRepository.Setup(c => c.Add(customer)).Returns(resultId);
            Assert.AreEqual(id, resultId);
        }

        [TestMethod]
        public void GetCustomer_Should_Return_Customer()
        {
            var customerRepository = new Mock<ICustomerRepository>();
            var id = Guid.NewGuid();
            var resultId = id;
            var customer = new Customer()
            {
                Id = id,
                Name = "",
                IsActive = true
            };
            customerRepository.Setup(c => c.GetCustomer(id)).Returns(customer);
            Assert.AreEqual(id, customer.Id);
        }

        
    }
}
