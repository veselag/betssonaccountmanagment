﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DataAccess.Interface;
using DataAccess.Model;
using Moq;

namespace BetssonAccountManagment
{
    [TestClass]
    public class AccountRepositoryTest
    {
        [TestMethod]
        public void Add_Should_Return_ID()
        {
            var accountRepository = new Mock<IAccountRepository>();
            var id = Guid.NewGuid();
            var resultId = id;
            var customerId = Guid.NewGuid();
            var account = new Account()
            {
                Id = id,
                Amount = 0.0M,
                IsClosed = false
            };
            
            accountRepository.Setup(c => c.Add(account)).Returns(resultId);
            Assert.AreEqual(id, resultId);
        }

        [TestMethod]
        public void Update_Account_Should_Return_Account()
        {
            {
                var accountRepository = new Mock<IAccountRepository>();
                var id = Guid.NewGuid();
                var customerId = Guid.NewGuid();
                var account = new Account()
                {
                    Id = id,
                    Amount = 0.0M,
                    IsClosed = false
                };
                var accountResult = new Account()
                {
                    Id = id,
                    Amount = 1.0M,
                    IsClosed = false
                };
                accountRepository.Setup(c => c.Update(account)).Returns(accountResult);
                Assert.AreEqual(account.Id, accountResult.Id);
            }
        }

        [TestMethod]
        public void GetAccount_Should_Return_Account()
        {
            var accountRepository = new Mock<IAccountRepository>();
            var id = Guid.NewGuid();
            var resultId = id;
            var account = new Account()
            {
                Id = id,
                Amount = 0.0M,
                IsClosed = false
            };
            accountRepository.Setup(c => c.Get(id)).Returns(account);
            Assert.AreEqual(id, account.Id);
        }

    }
}
