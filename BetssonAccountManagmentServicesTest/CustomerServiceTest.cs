﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BetssonAccountManagmentServices.Interface;

using Moq;

namespace BetssonAccountManagmentServicesTest
{
    [TestClass]
    public class CustomerServiceTest
    {
        [TestMethod]
        public void AddCustomer_Should_Return_ID()
        {
            var customerService = new Mock<ICustomerService>();
            var id = Guid.NewGuid();
            var resultId = id;
           
            customerService.Setup(c => c.Add(id)).Returns(resultId);
            Assert.AreEqual(id, resultId);
        }

    }
}
