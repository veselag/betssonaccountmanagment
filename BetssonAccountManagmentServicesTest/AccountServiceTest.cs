﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BetssonAccountManagmentServices.Interface;
using Moq;
using DataAccess.Interface;
using DataAccess.Model;
using BetssonAccountManagmentServices.Implementation;

namespace BetssonAccountManagmentServicesTest
{
    [TestClass]
    public class AccountServiceTest
    {
        
        [TestMethod]
        public void Close_Account_Should_Return_True()
        {
            var id = Guid.NewGuid();
            var account = new Account()
            {
                Id = id,
                IsClosed = false
            };
            var accountResult = new Account()
            {
                Id = id,
                IsClosed = true
            };
            var transfer = new Transfer()
            {
                Id = Guid.NewGuid(),
                AccountId = id,
                Amount = 0.0M,
                Type = TransferType.Credit,
                TransferTime = DateTime.Now,
                Status = TransferStatus.NotSaved,
            };
            var accountRepository = new Mock<IAccountRepository>();
            accountRepository.Setup(c => c.Get(id)).Returns(account);
            accountRepository.Setup(c => c.Update(account)).Returns(accountResult);
            var transferRepository = new Mock<ITransferRepository>();
            transferRepository.Setup(c => c.Add(transfer)).Returns(id);

            AccountService accountService = new AccountService(accountRepository.Object, transferRepository.Object);
            var IsClosed = accountService.Close(id);
            Assert.IsTrue(IsClosed);
        }

        [TestMethod]
        public void Close_Account_Should_Return_IsClosed()
        {
            try
            {
                var id = Guid.NewGuid();
                var account = new Account()
                {
                    Id = id,
                    IsClosed = true
                };
                var accountResult = new Account()
                {
                    Id = id,
                    IsClosed = true
                };
                var transfer = new Transfer()
                {
                    Id = Guid.NewGuid(),
                    AccountId = id,
                    Amount = 0.0M,
                    Type = TransferType.Credit,
                    TransferTime = DateTime.Now,
                    Status = TransferStatus.NotSaved,
                };
                var accountRepository = new Mock<IAccountRepository>();
                accountRepository.Setup(c => c.Get(id)).Returns(account);
                accountRepository.Setup(c => c.Update(account)).Returns(new Account());
                var transferRepository = new Mock<ITransferRepository>();
                transferRepository.Setup(c => c.Add(transfer)).Returns(id);

                AccountService accountService = new AccountService(accountRepository.Object, transferRepository.Object);
                var IsClosed = accountService.Close(id);
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is Exception);
            }
        }

        [TestMethod]
        public void GetBalance_Account_Should_Return_Value()
        {
            var id = Guid.NewGuid();
            var account = new Account()
            {
                Id = id,
                Amount = 5.0M,
                IsClosed = false
            };
           
            var transfer = new Transfer()
            {
                Id = Guid.NewGuid(),
                AccountId = id,
                Amount = 0.0M,
                Type = TransferType.Credit,
                TransferTime = DateTime.Now,
                Status = TransferStatus.NotSaved,
            };
            var accountRepository = new Mock<IAccountRepository>();
            accountRepository.Setup(c => c.Get(id)).Returns(account);
            var transferRepository = new Mock<ITransferRepository>();
            transferRepository.Setup(c => c.Add(transfer)).Returns(id);

            AccountService accountService = new AccountService(accountRepository.Object, transferRepository.Object);
            var balance = accountService.GetBalance(id);
            Assert.AreEqual(balance, 5);
        }

        [TestMethod]
        public void GetBalance_Account_Should_Return_IsClosed()
        {
            try
            {
                var id = Guid.NewGuid();
                var account = new Account()
                {
                    Id = id,
                    Amount = 5.0M,
                    IsClosed = true
                };

                var transfer = new Transfer()
                {
                    Id = Guid.NewGuid(),
                    AccountId = id,
                    Amount = 0.0M,
                    Type = TransferType.Credit,
                    TransferTime = DateTime.Now,
                    Status = TransferStatus.NotSaved,
                };
                var accountRepository = new Mock<IAccountRepository>();
                accountRepository.Setup(c => c.Get(id)).Returns(account);
                var transferRepository = new Mock<ITransferRepository>();
                transferRepository.Setup(c => c.Add(transfer)).Returns(id);

                AccountService accountService = new AccountService(accountRepository.Object, transferRepository.Object);
                var balance = accountService.GetBalance(id);
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is Exception);
            }
        }

        [TestMethod]
        public void DepositTransfer_Should_Return_Value()
        {
            var id = Guid.NewGuid();
            var account = new Account()
            {
                Id = id,
                Amount = 5.0M,
                IsClosed = false
            };
            var accountResult = new Account()
            {
                Id = id,
                Amount = 6.0M,
                IsClosed = false
            };
            var deposit = 1.0M;
            var transfer = new Transfer()
            {
                Id = Guid.NewGuid(),
                AccountId = id,
                Amount = 0.0M,
                Type = TransferType.Credit,
                TransferTime = DateTime.Now,
                Status = TransferStatus.NotSaved,
            };
            var accountRepository = new Mock<IAccountRepository>();
            accountRepository.Setup(c => c.Get(id)).Returns(account);
            accountRepository.Setup(c => c.Update(account)).Returns(accountResult);
            var transferRepository = new Mock<ITransferRepository>();
            transferRepository.Setup(c => c.Add(transfer)).Returns(id);

            AccountService accountService = new AccountService(accountRepository.Object, transferRepository.Object);
            var balance = accountService.DepositTransfer(id, deposit);
            Assert.AreEqual(balance, 6);
        }

        [TestMethod]
        public void DepositTransfer_Should_Return_IsClosed()
        {
            try
            {
                var id = Guid.NewGuid();
                var account = new Account()
                {
                    Id = id,
                    Amount = 5.0M,
                    IsClosed = true
                };
                var accountResult = new Account()
                {
                    Id = id,
                    Amount = 6.0M,
                    IsClosed = true
                };
                var deposit = 1.0M;
                var transfer = new Transfer()
                {
                    Id = Guid.NewGuid(),
                    AccountId = id,
                    Amount = 0.0M,
                    Type = TransferType.Credit,
                    TransferTime = DateTime.Now,
                    Status = TransferStatus.NotSaved,
                };
                var accountRepository = new Mock<IAccountRepository>();
                accountRepository.Setup(c => c.Get(id)).Returns(account);
                accountRepository.Setup(c => c.Update(account)).Returns(accountResult);
                var transferRepository = new Mock<ITransferRepository>();
                transferRepository.Setup(c => c.Add(transfer)).Returns(id);

                AccountService accountService = new AccountService(accountRepository.Object, transferRepository.Object);
                var balance = accountService.DepositTransfer(id, deposit);
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is Exception);
            }
        }

        [TestMethod]
        public void WithdrowTransfer_Should_Return_Value()
        {
            var id = Guid.NewGuid();
            var account = new Account()
            {
                Id = id,
                Amount = 5.0M,
                IsClosed = false
            };
            var accountResult = new Account()
            {
                Id = id,
                Amount = 4.0M,
                IsClosed = false
            };
            var withdrow = 1.0M;
            var transfer = new Transfer()
            {
                Id = Guid.NewGuid(),
                AccountId = id,
                Amount = 0.0M,
                Type = TransferType.Credit,
                TransferTime = DateTime.Now,
                Status = TransferStatus.NotSaved,
            };
            var accountRepository = new Mock<IAccountRepository>();
            accountRepository.Setup(c => c.Get(id)).Returns(account);
            accountRepository.Setup(c => c.Update(account)).Returns(accountResult);
            var transferRepository = new Mock<ITransferRepository>();
            transferRepository.Setup(c => c.Add(transfer)).Returns(id);

            AccountService accountService = new AccountService(accountRepository.Object, transferRepository.Object);
            var balance = accountService.WithdrowTransfer(id, withdrow);
            Assert.AreEqual(balance, 4);
        }

        [TestMethod]
        public void WithdrowTransfer_Should_Return_IsClosed()
        {
            try
            {
                var id = Guid.NewGuid();
                var account = new Account()
                {
                    Id = id,
                    Amount = 5.0M,
                    IsClosed = true
                };
                var accountResult = new Account()
                {
                    Id = id,
                    Amount = 4.0M,
                    IsClosed = true
                };
                var withdrow = 1.0M;
                var transfer = new Transfer()
                {
                    Id = Guid.NewGuid(),
                    AccountId = id,
                    Amount = 0.0M,
                    Type = TransferType.Credit,
                    TransferTime = DateTime.Now,
                    Status = TransferStatus.NotSaved,
                };
                var accountRepository = new Mock<IAccountRepository>();
                accountRepository.Setup(c => c.Get(id)).Returns(account);
                accountRepository.Setup(c => c.Update(account)).Returns(accountResult);
                var transferRepository = new Mock<ITransferRepository>();
                transferRepository.Setup(c => c.Add(transfer)).Returns(id);

                AccountService accountService = new AccountService(accountRepository.Object, transferRepository.Object);
                var balance = accountService.WithdrowTransfer(id, withdrow);
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is Exception);
            }
        }

        [TestMethod]
        public void WithdrowTransfer_Should_Return_Not_Enough()
        {
            try
            {
                var id = Guid.NewGuid();
                var account = new Account()
                {
                    Id = id,
                    Amount = 5.0M,
                    IsClosed = false
                };
                var accountResult = new Account()
                {
                    Id = id,
                    Amount = -1.0M,
                    IsClosed = false
                };
                var withdrow = 6.0M;
                var transfer = new Transfer()
                {
                    Id = Guid.NewGuid(),
                    AccountId = id,
                    Amount = 0.0M,
                    Type = TransferType.Credit,
                    TransferTime = DateTime.Now,
                    Status = TransferStatus.NotSaved,
                };
                var accountRepository = new Mock<IAccountRepository>();
                accountRepository.Setup(c => c.Get(id)).Returns(account);
                accountRepository.Setup(c => c.Update(account)).Returns(accountResult);
                var transferRepository = new Mock<ITransferRepository>();
                transferRepository.Setup(c => c.Add(transfer)).Returns(id);

                AccountService accountService = new AccountService(accountRepository.Object, transferRepository.Object);
                var balance = accountService.WithdrowTransfer(id, withdrow);
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is Exception);
            }
        }
    }
}
