﻿using DataAccess.Model;
using System;
namespace DataAccess.Interface
{
    public interface IAccountRepository
    {
        Guid Add(Account account);
        Account Get(Guid id);
        Account Update(Account account);
    }
}
