﻿using DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Model;
using DataAccess.Exceptions;

namespace DataAccess.Implementation
{
    public class CustomerRepository : ICustomerRepository
    {
        public Guid Add(Customer customer)
        {
            try
            {
                using (var context = new DataContext())
                {
                    var customerAdded = context.Customers.Add(customer);
                    context.SaveChanges();
                    return customerAdded.Id;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public Customer GetCustomer(Guid Id)
        {
            try
            {
                using (var context = new DataContext())
                {
                    var customer = context.Customers.SingleOrDefault(e => e.Id == Id);
                    if (customer != null)
                    {
                        return customer;
                    }
                    throw new CustomException(string.Format("Not found Custmer witd Id = {0}", Id));
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
