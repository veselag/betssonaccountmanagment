﻿using DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Model;

namespace DataAccess.Implementation
{
    class TransferRepository : ITransferRepository
    {
        public Guid Add(Transfer transfer)
        {
            try
            {
                using (var context = new DataContext())
                {
                    var transferAdded = context.Transfers.Add(transfer);
                    context.SaveChanges();
                    return transferAdded.Id;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        public TransferStatus Update(Transfer transfer)
        {
            try
            {
                using (var context = new DataContext())
                {
                    var transferObjects = context.Transfers.SingleOrDefault(e => e.Id == transfer.Id);
                    transferObjects.Status = transfer.Status;
                    context.SaveChanges();
                    return transfer.Status;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
    }
}
