﻿using DataAccess.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataAccess.Model;
using DataAccess.Exceptions;

namespace DataAccess.Implementation
{
    public class AccountRepository : IAccountRepository
    {
        public Guid Add(Account account)
        {
            try
            {
                using (var context = new DataContext())
                {
                    var accountAdded = context.Accounts.Add(account);
                    context.SaveChanges();
                    return accountAdded.Id;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }        

        public Account Get(Guid id)
        {
            try
            {
                using (var context = new DataContext())
                {
                    var account = context.Accounts.SingleOrDefault(e => e.Id == id);
                    if (account != null)
                    {
                        return account;
                    }
                    throw new CustomException(string.Format("The account with ID {0} does not exists"));
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
        
        public Account Update(Account account)
        {
            try
            {
                using (var context = new DataContext())
                {
                    var accountDB = context.Accounts.SingleOrDefault(e => e.Id == account.Id );
                    accountDB = account;
                    context.SaveChanges();
                    return account;
                }
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }        
    }
}
