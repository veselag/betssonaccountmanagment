﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Model
{
    public class Account
    {
        public Guid Id { get; set; }
        public decimal Amount { get; set; }
        public bool IsClosed { get; set; }
    }
}
