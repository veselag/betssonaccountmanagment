﻿using System.Data.Entity;

namespace DataAccess.Model
{
    public class DataContext : DbContext    {
       
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Transfer> Transfers { get; set; }



    }
}
