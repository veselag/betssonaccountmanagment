﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAccess.Model
{
    public enum TransferType
    {
        Credit,
        Debit
    }

    public enum TransferStatus
    {
        SavedOnServer,
        NotSaved
    }
    public class Transfer
    {
        public Guid Id { get; set; }
        public Guid AccountId { get; set; }
        public decimal Amount { get; set; }
        public TransferType Type { get; set; }
        public DateTime TransferTime { get; set; }
        public TransferStatus Status { get; set; }
    }
}
