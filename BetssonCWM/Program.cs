﻿using BetssonAccountManagmentServices.Implementation;
using BetssonAccountManagmentServices.Interface;
using DataAccess.Interface;
using System;

namespace BetssonCWM
{
    class Program
    {
        private static ICustomerService _customerService;
        private static ICustomerRepository _customerRepository;

        private static IAccountService _accountService;
        private static IAccountRepository _accountRepository;
        private static ITransferRepository _transferRepository;

        private static Guid _accountId;


        static void Main(string[] args)
        {
            _customerService = new CustomerService(_customerRepository);
            _accountService = new AccountService(_accountRepository, _transferRepository);

            //User story 1
            AddNewCustomer();

            //User story 2
            GetAccountBlance();

            //User story 3
            var withdrowAmount = 1.0M;
            WithdrowFunds(withdrowAmount);

            //User story 4
            var depositAmount = 1.0M;
            Deposit(depositAmount);

            //User story 5
            CloseAccount();

        }

        private static void CloseAccount()
        {
            try
            {

                if (_accountService.Close(_accountId))
                {
                    Console.WriteLine("Customer accountId is closed", _accountId);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void Deposit(decimal depositAmount)
        {
            try
            {
                var deposit = _accountService.DepositTransfer(_accountId, depositAmount);
                Console.WriteLine("Deposit is succssesfull");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void WithdrowFunds(decimal amount)
        {
            try
            {
                Guid customeID = Guid.NewGuid();
                var transferID = _accountService.WithdrowTransfer(_accountId, amount);
                Console.WriteLine("Withdrow Fundsis is succssesfull");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void GetAccountBlance()
        {
            try
            {
                Guid customeID = Guid.NewGuid();
                var balance = _accountService.GetBalance(_accountId);
                Console.WriteLine("Account balance is {0}", balance);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void AddNewCustomer()
        {
            try
            {
                Guid customeId = Guid.NewGuid();
                _accountId = _customerService.Add(customeId);
                Console.WriteLine("Customer accountId is {0}", _accountId);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
