﻿using System;

namespace BetssonAccountManagmentServices.Interface
{
    public interface IAccountService
    {
        decimal GetBalance(Guid accountId);
        decimal WithdrowTransfer(Guid accountId, decimal amount);
        decimal DepositTransfer(Guid accountId, decimal amount);
        bool Close(Guid accountId);
    }
}
