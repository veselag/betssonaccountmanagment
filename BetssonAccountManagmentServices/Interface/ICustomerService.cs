﻿using System;

namespace BetssonAccountManagmentServices.Interface
{
    public interface ICustomerService
    {
        Guid Add(Guid customerId);
    }
}
