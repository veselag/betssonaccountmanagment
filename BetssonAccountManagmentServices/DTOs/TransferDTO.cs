﻿using System;

namespace BetssonAccountManagmentServices.DTO
{
    public enum TransferType
    {
        Credit,
        Debit
    }

    public enum TransferStatus
    {
        SavedOnServer,
        NotSaved
    }
    public class TransferDTO
    {
        public Guid Id { get; set; }
        public Guid AccountId { get; set; }
        public decimal Amount { get; set; }
        public TransferType Type { get; set; }
        public DateTime TransferTime { get; set; }
        public TransferStatus Status { get; set; }
    }
}
