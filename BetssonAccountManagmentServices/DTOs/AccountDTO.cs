﻿using System;

namespace BetssonAccountManagmentServices.DTO
{
    public class AccountDTO
    {
        public Guid Id { get; set; }
        public decimal Amount { get; set; }
        public bool IsActive { get; set; }
    }
}
