﻿using System;

namespace BetssonAccountManagmentServices.DTO
{
    public class CustomerDTO
    {
        public Guid Id { get; set; }
        public Guid AccountId { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
