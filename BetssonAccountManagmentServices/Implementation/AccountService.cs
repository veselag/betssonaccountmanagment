﻿using BetssonAccountManagmentServices.Exceptions;
using BetssonAccountManagmentServices.Interface;
using DataAccess.Interface;
using DataAccess.Model;
using System;

namespace BetssonAccountManagmentServices.Implementation
{
    public class AccountService : IAccountService
    {
        private IAccountRepository _accountRepository;
        private ITransferRepository _tranferRepository;

        public AccountService(IAccountRepository accountRepository, ITransferRepository tranferRepository)
        {
            _accountRepository = accountRepository;
            _tranferRepository = tranferRepository;
        }

        public bool Close(Guid accountId)
        {
            try
            {
                var account = _accountRepository.Get(accountId);
                if (!IsClosedOrNonExcists(account))
                {
                    if (account.Amount == 0)
                    {
                        account.IsClosed = true;
                        _accountRepository.Update(account);
                        return true;
                    }
                    throw new CustomException(string.Format("The account with ID {0} is not empty", accountId));
                }
                throw new CustomException(string.Format("The account with Id {0} does not exists or is closed", accountId));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal DepositTransfer(Guid accountId, decimal amount)
        {
            var transfer = new Transfer()
            {
                Id = Guid.NewGuid(),
                AccountId = accountId,
                Amount = amount,
                Type = TransferType.Credit,
                TransferTime = DateTime.Now,
                Status = TransferStatus.SavedOnServer,
            };
            try {
                var account = _accountRepository.Get(accountId);
                if (!IsClosedOrNonExcists(account))
                {
                    account.Amount = account.Amount + amount;
                    _accountRepository.Update(account);
                    return account.Amount;
                }
                throw new CustomException(string.Format("The account with Id {0} does not exists or is closed", accountId));
            }
            catch (Exception ex)
            {
                transfer.Status = TransferStatus.NotSaved;
                _tranferRepository.Add(transfer);
                throw ex;
            }
        }

        public decimal GetBalance(Guid accountId)
        {
            try
            {
                var account = _accountRepository.Get(accountId);
                if (!IsClosedOrNonExcists(account))
                {
                    return account.Amount;
                }
                throw new CustomException(string.Format("The account with Id {0} does not exists or is closed", accountId));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool IsClosedOrNonExcists(Account account)
        {
            try
            {
                if (account != null)
                {
                    if (account.IsClosed)
                    {
                        return true;
                    }
                    return false;
                }
                return true;                
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public decimal WithdrowTransfer(Guid accountId, decimal amount)
        {
            var transfer = new Transfer()
            {
                Id = Guid.NewGuid(),
                AccountId = accountId,
                Amount = amount,
                Type = TransferType.Credit,
                TransferTime = DateTime.Now,
                Status = TransferStatus.SavedOnServer,
            };
            try
            {
                var account = _accountRepository.Get(accountId);
                
                if (!IsClosedOrNonExcists(account))
                {
                    var newAmount = account.Amount - amount;
                    if (newAmount > 0)
                    {
                        account.Amount = newAmount;
                        _accountRepository.Update(account);
                        return newAmount;
                    }
                    else
                    {
                        transfer.Status = TransferStatus.NotSaved;
                        _tranferRepository.Add(transfer);
                        throw new Exception("The balance is not enough");
                    }
                }
                throw new CustomException(string.Format("The account with Id {0} does not exists or is closed", accountId));
            }
            catch (Exception ex)
            {
                transfer.Status = TransferStatus.NotSaved;
                _tranferRepository.Add(transfer);
                throw ex;
            }
        }
    }
}
