﻿using BetssonAccountManagmentServices.Exceptions;
using BetssonAccountManagmentServices.Interface;
using DataAccess.Interface;
using DataAccess.Model;
using System;

namespace BetssonAccountManagmentServices.Implementation
{
    public class CustomerService : ICustomerService
    {
        private ICustomerRepository _customerRepository;
        public CustomerService(ICustomerRepository  customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public Guid Add(Guid customerId)
        {
            var accountId = new Guid();
            try
            {
                if (_customerRepository.GetCustomer(customerId) != null)
                {
                    accountId = Guid.NewGuid();
                    var customer = new Customer()
                    {
                        Id = customerId,
                        AccountId = accountId,
                        IsActive = true
                    };
                    _customerRepository.Add(customer);
                    return accountId;
                }               
                throw new CustomException(string.Format("The customer with Id = {0} does not excist", customerId));

            }
            catch (Exception ex)
            {
                throw ex;
            }
            
        }
    }
}
